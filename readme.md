﻿# Confluence-Ever-Edit

Kleines Chrome-Plugin, welches den Edit-Button im Confluence immer anzeigt. Man muss also nicht mehr hochscrollen, um eine Seite zu editieren.
Für alle Mausnutzer! :)

## Installation

* Öffne ``chrome://extensions``
* Hacken bei ``Entwicklermodus`` setzen
* Über Button ``Entpackte Erweiterung laden...`` das Plugin installieren
    * Einfach ausgechecktes Verzeichnis auswählen